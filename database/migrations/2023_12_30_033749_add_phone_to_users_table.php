<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('last-name')->after('password');
            $table->string('phone')->after('last-name');
            $table->string('dob')->after('phone');
            $table->string('address')->after('dob');
            $table->string('gender')->after('address');
            $table->string('resume_name')->after('gender');
            $table->string('resume_path')->after('resume_name');
            $table->string('photo_name')->after('resume_path');
            $table->string('photo_path')->after('photo_name');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('last-name');
            $table->dropColumn('phone');
            $table->dropColumn('dob');
            $table->dropColumn('address');
            $table->dropColumn('gender');
            $table->dropColumn('resume_name');
            $table->dropColumn('resume_path');
            $table->dropColumn('photo_name');
            $table->dropColumn('photo_path');
        });
    }
};
