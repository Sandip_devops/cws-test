<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('login');
})->name('login');
Route::get('/register', function () {
    return view('register');
});
Route::post('register-user', [UserController::class, 'create']);
Route::get('check-email-unique', [UserController::class, 'emailCheck']);

Route::group(['middleware' => ['Checkuser']], function(){
    Route::post('index', [UserController::class, 'index']);
    Route::get('/user-list', [UserController::class, 'userList'])->name('user-list');
    Route::get('showdata', [UserController::class, 'show'])->name('showdata');
    Route::get('edit', [UserController::class, 'edit'])->name('edit');
    Route::post('update-user', [UserController::class, 'update']);
    Route::get('delete-user', [UserController::class, 'userDelete'])->name('delete-user');  
});


