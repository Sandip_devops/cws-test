<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use DataTables;

class UserController extends Controller
{
    public function index(Request $request){
        return redirect()->route('user-list');
    }

    public function userList(Request $request){
        $title = "Fee";

        if ($request->ajax()) {

            $data = User::get();
            return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('name', function($row){
                return $row->name ?? "";
            })->addColumn('email', function($row){
                return $row->email ?? "";
            })->addColumn('phone', function($row){
                return $row->phone ?? "";
            })->addColumn('action', function($row){
                $btn = '<a class="btn btn-primary btn-sm mr-1" onClick="showData('.$row->id.');"><i class="fa fa-eye"></i></a>';
                $btn = $btn.'<a class="btn btn-warning btn-sm mr-1" id="'.$row->id.'" style="margin-right:5px;" onClick="edit('.$row->id.');"><i class="fa fa-edit"></i></a>';
                $btn = $btn. '<a class="btn btn-danger btn-sm mr-1" onClick="deleteUser('.$row->id.');"><i class="fa fa-trash"></i></a>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }

        return view('userlist');
    }
    
    public function create(Request $request){
        $validator = Validator::make($request->all(), [
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'mobile' => 'required|numeric|digits:10', // Assuming mobile is a string; use 'integer' if it should be numeric.
            'email' => 'required|email',
            'dob' => 'required|date', // Assuming dob is a date; adjust the format if needed.
            'address' => 'required|string',
            'gender' => 'required|string|in:male,female', // Assuming gender is a string with specific values.
            'resume' => 'required|file|mimes:pdf,docx|max:2048', // Assuming resume is a file with specific allowed mime types.
            'photo' => 'required|mimes:png,jpg|max:2048', // Assuming photo is an image with specific allowed mime types.
        ]);
        
 
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();

        }else {
            // dd($request);
            $photo_name = '';
            $photo_path = '';
            $resume_name = '';
            $resume_path = '';
           
            $photo_path = public_path().'/photo';
            $image = $request->crp_photo;
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = time().'_'.$request->firstname.'_'.uniqid().'.png';

            $upload_photo = file_put_contents($photo_path.'/'.$imageName, base64_decode($image));

            if(!$upload_photo){
                return redirect()->back();
            }

            if ($request->hasFile('resume')){
                $file = $request->file('resume');
                $extension = $file->getClientOriginalExtension(); // you can also use file name
                $resume_name = time().'_'.$request->firstname.'_.'.$extension;
                $resume_path = public_path().'/photo';
                $uplaod = $file->move($resume_path,$resume_name);
                if(!$uplaod){
                    return redirect()->back();
                }
            }
          
            $flight = User::create([
                'name' => $request->firstname,
                'last-name' => $request->lastname,
                'phone' => $request->mobile,
                'password' => Hash::make($request->mobile),
                'dob' => $request->dob,
                'email' => $request->email,
                'address' => $request->address,
                'gender' => $request->gender,
                'resume_name' => $resume_name,
                'resume_path' => $resume_path,
                'photo_name' => $imageName,
                'photo_path' => $photo_path,
            ]);

            if($flight){
                return redirect()->route('user-list');
            } else {
                return redirect()->back()
                ->withErrors("ERRRRRR")
                        ->withInput();
            }
        }
    }

    public function emailCheck(Request $request){
        $emailfount = User::where('email', $request->email)->first();
        if($emailfount){
            return response()->json('This email already exist!!');
        }else {
            return response()->json('true');
        }
    }

    public function show(Request $request){
        $user_check = User::where('id', $request->userId)->first();
        if($user_check){
            return view('ajax.show', compact('user_check'));
        }else {
            return response()->json(['status' => 403, 'message' =>'Not fonud!!']);
        }

    }

    public function edit(Request $request){
        $user_check = User::where('id', $request->userId)->first();
        if($user_check){
            return view('ajax.edit', compact('user_check'));
        }else {
            return response()->json(['status' => 403, 'message' =>'Not fonud!!']);
        }
    }

    public function update(Request $request){
        // dd($request);
        $photo_name = '';
        $photo_path = '';
        $resume_name = '';
        $resume_path = '';
       
        $photo_path = public_path().'/photo';
        $image = $request->crp_photo;
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = time().'_'.$request->firstname.'_'.uniqid().'.png';

        $upload_photo = file_put_contents($photo_path.'/'.$imageName, base64_decode($image));

        if ($request->hasFile('resume')){
            $file = $request->file('resume');
            $extension = $file->getClientOriginalExtension(); // you can also use file name
            $resume_name = time().'_'.$request->firstname.'_.'.$extension;
            $resume_path = public_path().'/photo';
            $uplaod = $file->move($resume_path,$resume_name);
            if(!$uplaod){
                return redirect()->back();
            }
        }

        $updated = User::where('id', $request->id)
            ->update(
                [
                'name' => $request->name,
                'last-name' => $request->lastName,
                'phone' => $request->phone,
                'password' => Hash::make($request->phone),
                'dob' => $request->dob,
                'email' => $request->email,
                'address' => $request->address,
                'gender' => $request->gender,
                ]
            );

        if($updated){
            toastr()->success('User updated successfuly!!');
            return back();
        }else {
            toastr()->error('An error has occurred please try again later.');
            return back();
        }
    }

    public function userDelete(Request $request){
        $user_check = User::where('id', $request->userId)->first();
        if($user_check){
            $user_check = User::where('id', $request->userId)->delete();
            if($user_check){
                return response()->json(['status' => 200, 'message' =>'User deleted successfuly!!']);
            } else {
                return response()->json(['status' => 400, 'message' =>'Something went to wrong!!']);
            }
        }else {
            return response()->json(['status' => 403, 'message' =>'Not fonud!!']);
        }

    }
}
