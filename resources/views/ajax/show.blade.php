<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Show User</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<div class="modal-body">
    <div class="row g-3">
        <div class="col-md-6">
            <label for="name" class="form-label">First Name</label>
            <input type="text" value="{{$user_check->name}}" readonly>
        </div>
        <div class="col-md-6">
            <label for="lastName" class="form-label">Last Name</label>
            <input type="text" value="{{$user_check->{'last-name'} }}" readonly>
        </div>
        <div class="col-md-6">
            <label for="name" class="form-label">Phone</label>
            <input type="text" value="{{$user_check->phone}}" readonly>
        </div>
        <div class="col-md-6">
            <label for="lastName" class="form-label">Email</label>
            <input type="text" value="{{$user_check->email}}" readonly>
        </div>
        <div class="col-md-6">
            <label for="name" class="form-label">DOB</label>
            <input type="text" value="{{$user_check->dob}}" readonly>
        </div>
        <div class="col-md-6">
            <label for="lastName" class="form-label">ADDRESS</label>
            <input type="text" value="{{$user_check->address}}" readonly>
        </div>
        <div class="col-md-6">
            <label for="name" class="form-label">Gender</label>
            <input type="text" value="{{$user_check->gender}}" readonly>
        </div>
        <div class="col-md-6">
            <label for="lastName" class="form-label">Resume</label>
            <a href="{{ asset('photo/' . $user_check->resume_name) }}" target="_blank">View Resume</a>
        </div>
        <div class="col-md-6">
            <label for="name" class="form-label">Photo</label>
            <img src="{{ asset('photo/' . $user_check->photo_name) }}" alt="User Photo">
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
</div>
