<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Show User</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<div class="modal-body">
    <form class="row g-3" id="registrationForm" action="update-user" method="post" enctype="multipart/form-data">
        @csrf
        <div class="col-md-6">
            <label for="name" class="form-label">First Name</label>
            <input type="text" name="name" value="{{$user_check->name}}">
            <input type="hidden" name="id" value="{{$user_check->id}}">
        </div>
        <div class="col-md-6">
            <label for="lastName" class="form-label">Last Name</label>
            <input type="text" name="lastName" value="{{$user_check->{'last-name'} }}">
        </div>
        <div class="col-md-6">
            <label for="phone" class="form-label">Phone</label>
            <input type="text" name="phone" value="{{$user_check->phone}}">
        </div>
        <div class="col-md-6">
            <label for="email" class="form-label">Email</label>
            <input type="text" name="email" value="{{$user_check->email}}">
        </div>
        <div class="col-md-6">
            <label for="dob" class="form-label">YOUR DOB</label>
            <input type="text" name="dob" value="{{$user_check->dob}}">
        </div>
        <div class="col-md-6">
            <label for="address" class="form-label">ADDRESS</label>
            <input type="text" name="address" value="{{$user_check->address}}">
        </div>
        <div class="col-md-6">
            <label for="gender" class="form-label">Gender</label>
            <!-- <input type="text" name="gender" value="{{$user_check->gender}}"> -->
            @if($user_check->gender == 'male')
            <div class="form-check">
                <input class="form-check-input" type="radio" name="gender" id="male" value="male" required checked>
                <label class="form-check-label" for="male">Male</label>
            </div>
            
            <div class="form-check">
                <input class="form-check-input" type="radio" name="gender" id="female" value="female"
                    required>
                <label class="form-check-label" for="female">Female</label>
            </div>
            @else
            
            <div class="form-check">
                <input class="form-check-input" type="radio" name="gender" id="male" value="male" required>
                <label class="form-check-label" for="male">Male</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="gender" id="female" value="female"
                    required checked>
                <label class="form-check-label" for="female">Female</label>
            </div>
            @endif
        </div>
        <div class="col-md-6">
            <label for="ressume" class="form-label">Resume</label>
            <input type="file" class="form-control" name="resume" id="resume" accept=".pdf, .docx">
            <a href="{{ asset('photo/' . $user_check->resume_name) }}" target="_blank">View Resume</a>
        </div>
        <div class="col-md-6">
            <label for="photo" class="form-label">Photo</label>
            <input type="file" class="form-control image" name="photo" id="photo" accept=".jpg, .png">
            <img src="{{ asset('photo/' . $user_check->photo_name) }}" alt="User Photo">

        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </from>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
</div>