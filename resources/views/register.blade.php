<!DOCTYPE html>
<html>

<head>
    <title>Registration</title>
    <meta name="_token" content="{{ csrf_token() }}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>
</head>
<style type="text/css">
    img {
        display: block;
        max-width: 100%;
    }

    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }

    .modal-lg {
        max-width: 1000px !important;
    }
</style>

<body>

    <div class="container mt-5">
        <div class="card">
            <h2 class="card-header">User Details</h2>
            <div class="card-body">
                @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form class="row g-3" id="registrationForm" action="register-user" method="post"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-6">
                        <label for="firstname" class="form-label">First Name</label>
                        <input type="text" class="form-control" name="firstname" id="firstname"
                            placeholder="Enter your First Name" required>
                    </div>

                    <div class="col-md-6">
                        <label for="lastname" class="form-label">Last Name</label>
                        <input type="text" class="form-control" name="lastname" id="lastname"
                            placeholder="Enter your Last Name" required>
                    </div>

                    <div class="col-md-4">
                        <label for="mobile" class="form-label">Phone</label>
                        <input type="number" class="form-control" name="mobile" id="mobile"
                            placeholder="Enter your Mobile Number" required>
                    </div>

                    <div class="col-md-4">
                        <label for="email" class="form-label">Email-id</label>
                        <input type="email" class="form-control" name="email" id="email"
                            placeholder="Enter your email-id" required>
                    </div>


                    <div class="col-md-4">
                        <label for="dob" class="form-label">DOB</label>
                        <input type="date" class="form-control" name="dob" id="dob" placeholder="DOB" required>
                    </div>

                    <div class="col-md-6">
                        <label for="address" class="form-label">Address</label>
                        <textarea class="form-control" name="address" id="address" rows="4"
                            placeholder="Enter your address"></textarea>
                    </div>

                    <div class="col-md-6">
                        <label class="form-label">Gender</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="gender" id="male" value="male" required>
                            <label class="form-check-label" for="male">Male</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="gender" id="female" value="female"
                                required>
                            <label class="form-check-label" for="female">Female</label>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label for="resume" class="form-label">Resume</label>
                        <input type="file" class="form-control" name="resume" id="resume" accept=".pdf, .docx" required>
                    </div>

                    <div class="col-md-6">
                        <label for="photo" class="form-label">Photo</label>
                        <input type="file" class="form-control image" name="photo" id="photo" accept=".jpg, .png" required>
                        <div class="col-md-6" id="image1">
                        </div>
                        <!-- <input type="file" #file formControlName="imageName"> -->
                        <input type="file" name="cropped_photo" id="cropped_photo" hidden>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </from>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Laravel Cropper Js - Crop Image Before Upload - Tutsmake.com
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="img-container">
                        <div class="row">
                            <div class="col-md-8">
                                <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                            </div>
                            <div class="col-md-4">
                                <div class="preview"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" (change)="onFileChange($event);" class="btn btn-primary" id="crop">Crop</button>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/additional-methods.min.js"></script> -->
    <script>
        $(document).ready(function () {
            $("#registrationForm").validate({
                rules: {
                    email: {
                        required: true,
                        email: true,
                        remote: {
                            url: "check-email-unique", // Replace with your server-side endpoint
                            type: "get",
                            data: {
                                email: function () {
                                    return $("#email").val();
                                }
                            }
                        }
                    },
                    dob: {
                        required: true,
                        dateOfBirth: true,
                    },
                },
                messages: {
                    dob: {
                        dateOfBirth: "You must be 18 years or older.",
                    },
                    email: {
                        required: "Please enter your email",
                        email: "Please enter a valid email address",
                        remote: "This email is already taken"
                    },
                },
            });

            // Add custom method for date of birth validation
            $.validator.addMethod("dateOfBirth", function (value, element) {
                var dob = new Date(value);
                var today = new Date();
                var age = today.getFullYear() - dob.getFullYear();

                return age >= 18;
            }, "You must be 18 years or older.");
        });
    </script>

    <script>
        var $modal = $('#modal');
        var image = document.getElementById('image');
        var cropper;

        $("body").on("change", ".image", function (e) {
            var files = e.target.files;
            var done = function (url) {
                image.src = url;
                $modal.modal('show');
            };
            var reader;
            var file;
            var url;
            if (files && files.length > 0) {
                file = files[0];
                if (URL) {
                    done(URL.createObjectURL(file));
                } else if (FileReader) {
                    reader = new FileReader();
                    reader.onload = function (e) {
                        done(reader.result);
                    };
                    reader.readAsDataURL(file);
                }
            }
        });
        $modal.on('shown.bs.modal', function () {
            cropper = new Cropper(image, {
                aspectRatio: 1,
                viewMode: 3,
                preview: '.preview'
            });
        }).on('hidden.bs.modal', function () {
            cropper.destroy();
            cropper = null;
        });
        $("#crop").click(function () {
            canvas = cropper.getCroppedCanvas({
                width: 160,
                height: 160,
            });
            canvas.toBlob(function (blob) {
                url = URL.createObjectURL(blob);
                var reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = function () {
                    var base64data = reader.result;
                    $modal.modal('hide');
                    $("#image1").html('<img src=' + base64data + '> <input type="hidden" id="crp_photo" name="crp_photo" value=' + base64data + '>');
                }
            });
        })
    </script>
</body>

</html>