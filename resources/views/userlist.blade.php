<!DOCTYPE html>
<html>

<head>
    <title>Registration</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
    <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
    <!-- <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<style type="text/css">
    img {
        display: block;
        max-width: 100%;
    }

    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }

    .modal-lg {
        max-width: 1000px !important;
    }
</style>

<body>
    <div class="container mt-5">
        <div class="card">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h2 class="mr-auto">User list</h2>
            <a href="register" class="btn btn-primary ml-auto"><i class="fa fa-plus"></i> Add User</a>
        </div>
            
            <div class="card-body">
            <table id="myTable" class="table table-bordered">
                <thead>
                    <tr>
                        <th>S.No.</th>
                        <th>User Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</body>
<div class="modal fade" id="myModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content content-modal">
      
    </div>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>


<script type="text/javascript">

    $(document).ready(function(){

        $('#myTable').DataTable({

            processing: true,

            serverSide: true,

            ajax: "{{ route('user-list') }}",

            columns: [

                {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: true},

                {data: 'name', name: 'name'},

                {data: 'email', name: 'email'},

                {data: 'phone', name: 'phone'},

                {data: 'action', name: 'action'},

            ]

        });

    });

    function showData23232(id){
        
        $('#myModal').modal('show');
    }

    function showData(user_id){
        $.ajax({
            url: 'showdata',
            type: 'get',
            data: {'userId': user_id},
            success: function (data) {
                $('.content-modal').html(data);
                $('#myModal').modal('show');
            },
            error: function (xhr, status, error) {
                alert('Error deleting user: ' + xhr.responseText);
            }
        });
    }

    function edit(user_id){
        $.ajax({
            url: 'edit',
            type: 'get',
            data: {'userId': user_id},
            success: function (data) {
                $('.content-modal').html(data);
                $('#myModal').modal('show');
                // alert(data.message);
                // You might want to handle UI updates here, e.g., removing the deleted user from the page.
            },
            error: function (xhr, status, error) {
                alert('Error deleting user: ' + xhr.responseText);
            }
        });
    }

    function deleteUser(user_id){
        if (confirm('Are you sure you want to delete this user?')) {
            $.ajax({
                url: 'delete-user',
                type: 'get',
                data: {'userId': user_id},
                success: function (data) {
                    alert(data.message);
                    // You might want to handle UI updates here, e.g., removing the deleted user from the page.
                },
                error: function (xhr, status, error) {
                    alert('Error deleting user: ' + xhr.responseText);
                }
            });
        }
    }

</script>
</html>